import React from "react";
import { Route, Switch } from "react-router";
import { NavbarAccount } from "../components/ui/NavbarAccount";
import { ChooseLevelScreen } from "../components/Account/ChooseLevelScreen";
import { CreateAccountScreen } from "../components/Account/CreateAccountScreen";
import { RecoveryPassScreen } from "../components/Account/RecoveryPassScreen";

export const AccountRoutes = () => {
  return (
    <div>
      <NavbarAccount />
      <div>
        <Switch>
          <Route exact path="/account/signup" component={CreateAccountScreen} />
          <Route
            exact
            path="/account/recovery"
            component={RecoveryPassScreen}
          />
          <Route
            exact
            path="/account/choose-level"
            component={ChooseLevelScreen}
          />
        </Switch>
      </div>
    </div>
  );
};

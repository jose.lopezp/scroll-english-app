import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { LoginScreen } from "../components/Login/LoginScreen";
import { NotFoundScreen } from "../components/NotFount/NotFoundScreen";
import { AccountRoutes } from "./AccountRoutes";
import { DashboardRoutes } from "./DashboardRoutes";

export const AppRouter = () => {
  return (
    <Router>
      <div>
        <Switch>
            <Route exact path="/login" component={ LoginScreen }/>
            <Route exact path="/" component={ LoginScreen }/>
            <Route path="/account" component={ AccountRoutes }/>
            <Route path="/" component={ DashboardRoutes }/>
            <Route path="*" component={ NotFoundScreen }/>
        </Switch>
      </div>
    </Router>
  );
};

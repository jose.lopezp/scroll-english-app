import React from "react";
import { Route, Switch } from "react-router";
import { Navbar } from "../components/ui/Navbar";
import { ChangeLevelScreen } from "../components/Profile/ChangeLevelScreen";
import { ChangePassScreen } from "../components/Profile/ChangePassScreen";
import { GeneralInformationScreen } from "../components/Profile/GeneralInformationScreen";
import { HomeScren } from "../components/Timeline/HomeScren";
import { SocialScreen } from "../components/Timeline/SocialScreen";

export const DashboardRoutes = () => {
  return (
    <div>
      <Navbar />
      <div>
        <Switch>
          <Route exact path="/home" component={HomeScren} />
          <Route exact path="/social" component={SocialScreen} />
          <Route
            exact
            path="/profile/general-info"
            component={GeneralInformationScreen}
          />
          <Route
            exact
            path="/profile/change-pass"
            component={ChangePassScreen}
          />
          <Route
            exact
            path="/profile/change-level"
            component={ChangeLevelScreen}
          />
        </Switch>
      </div>
    </div>
  );
};

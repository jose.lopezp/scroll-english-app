import React from 'react'

export const NotFoundScreen = () => {
    return (
        <div>
            <h1>404 Not Found</h1>
        </div>
    )
}

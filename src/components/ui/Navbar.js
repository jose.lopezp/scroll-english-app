import React from "react";
import { Link} from "react-router-dom";

export const Navbar = () => {
  return (
    <nav>
      <Link className="navbar-brand" exact to="/login">
        Scroll
      </Link>
      <Link className="navbar-brand" exact to="/home">
        Home
      </Link>
      <Link className="navbar-brand" exact to="/social">
        Social
      </Link>
      <Link className="navbar-brand" exact to="/profile/general-info">
        Perfil
      </Link>
    </nav>
  );
};
